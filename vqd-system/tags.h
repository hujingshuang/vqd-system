#ifndef _TAGS_H
#define _TAGS_H

#include <vector>
#include <opencv2/highgui/highgui.hpp>

class Tags {
public:
    void addTags(cv::Mat& frame, std::vector<int> result) {
        putText(frame, "H", cvPoint(0, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        putText(frame, "J", cvPoint(60, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        putText(frame, "M", cvPoint(120, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        putText(frame, "P", cvPoint(180, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        putText(frame, "X", cvPoint(240, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        putText(frame, "G", cvPoint(300, 20), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));

        if (result.at(1))
            putText(frame, "Y", cvPoint(0, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        if (result.at(2))
            putText(frame, "Y", cvPoint(60, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        if (result.at(3))
            putText(frame, "Y", cvPoint(120, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        if (result.at(4))
            putText(frame, "Y", cvPoint(180, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        if (result.at(5))
            putText(frame, "Y", cvPoint(240, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
        if (result.at(6))
            putText(frame, "Y", cvPoint(300, 50), cv::FONT_HERSHEY_DUPLEX, 1.0, CV_RGB(0, 0, 255));
    }
};

#endif // _TAGS_H