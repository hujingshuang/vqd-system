﻿#include "Method.h"
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;
using namespace vqd;

const float Method::EPS = 0.000001F;
/**********************************************************/
/*                        黑屏异常检测                     */
/**********************************************************/
int BlackScreen::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray;
    float blackJudgeThreshold = 0.99F;                              // 比重超过99%的视为黑屏

    if (imageColor.channels() == 3) {
        cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);
    }
    else {
        imageGray = imageColor;
        blackJudgeThreshold = 0.98F;
    }

    const float blackThreshold = 5;                                 // 黑点像素阈值

    const int channels = 0;                                         // 灰度图像
    const int histSize = 2;                                         // 两部分
    const float hranges[3] = { 0, blackThreshold, 255 };                  // 0, blackThreshold, 255
    const float* ranges[1] = { hranges };
    Mat hist;                                                       // 直方统计结果

    calcHist(&imageGray, 1, &channels, Mat(), hist, 1, &histSize, ranges, false);

    const float blackNums = hist.at<float>(0);                            // 黑点数
    const float otherNums = hist.at<float>(1);                            // 非黑点数
    const float blackPercent = blackNums / (blackNums + otherNums + this->EPS); // 黑点数百分比
    //const float blackJudgeThreshold = 0.99F;                        // 比重超过99%的视为黑屏

    return (blackPercent >= blackJudgeThreshold) ? VQD_BLACK : VQD_NORMAL;
}
/**********************************************************/
/*                         静帧检测                        */
/**********************************************************/
int FrameFreeze::detecteMultiImage(const Mat& imageColorPrior, const Mat& imageColorNext) {
    Mat imageGrayPrior, imageGrayNext;
    cvtColor(imageColorPrior, imageGrayPrior, COLOR_BGR2GRAY);
    cvtColor(imageColorNext, imageGrayNext, COLOR_BGR2GRAY);

    Mat imageGrayDiff;
    absdiff(imageGrayPrior, imageGrayNext, imageGrayDiff);              // 差分绝对值

    BlackScreen blackscreen;

    return (blackscreen.detecte(imageGrayDiff) == VQD_BLACK) ? VQD_FREEZE : VQD_NORMAL;
}
/**********************************************************/
/*                         模糊检测                        */
/**********************************************************/
int FocusBlur::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray, imageLaplacian, imageAbsLaplacian;
    cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);

    const int kernelSize = 3;
    Laplacian(imageGray, imageLaplacian, CV_16S, kernelSize);       // 拉普拉斯变换，CV_16S
    convertScaleAbs(imageLaplacian, imageAbsLaplacian);             // CV_8U
    
    const Scalar mean, stddev;
    meanStdDev(imageAbsLaplacian, mean, stddev);                    // 方差
    
    const float blurThreshold = 15.0F;                              // 模糊度阈值
    const float  blurLowDegree = 0.5F;
    const double blurDegree = stddev.val[0];
    return ((blurDegree  > blurLowDegree) && (blurDegree < blurThreshold)) ? VQD_BLUR : VQD_NORMAL;
}
/**********************************************************/
/*                   雪花/噪声异常检测                     */
/**********************************************************/
/*
int SnowNoise::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray, imageBlur;
    const int kernelSize = 3;
    cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);
    medianBlur(imageGray, imageBlur, kernelSize);           // 中值滤波

    Mat imageDiff;
    subtract(imageGray, imageBlur, imageDiff);              // 差分（差分绝对值：absdiff）

    const Scalar mean, stddev;
    meanStdDev(imageDiff, mean, stddev);                    // 均值，方差
    const double sigma = stddev.val[0];

    const double snowNoiseThreshold = 1.0;                  // 雪花噪声阈值

    return (snowNoiseThreshold > snowNoiseThreshold) ? VQD_SNOW : VQD_NORMAL;
}
*/
int SnowNoise::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray, imageBlur;
    const int kernelSize = 3;

    cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);
    medianBlur(imageGray, imageBlur, kernelSize);                   // 中值滤波

    Mat imageDiff;
    absdiff(imageGray, imageBlur, imageDiff);                  // 绝对差分图像(差分subtract)

    const float diffThreshold = 30;                                 // 差分阈值

    const int channels = 0;                                         // 灰度图像
    const int histSize = 2;                                         // 两部分

    const float hranges[3] = { 0, diffThreshold, 255 };                   // 0, diffThreshold, 255
    const float* ranges[1] = { hranges };
    Mat hist;                                                       // 直方统计结果

    calcHist(&imageDiff, 1, &channels, Mat(), hist, 1, &histSize, ranges, false);
    const int snowNums = hist.at<int>(1);                                 // 超过阈值视为雪花噪声
    
    const int snowThreshold = 2000;

    if (snowNums < snowThreshold) {                                 // 初步检测，没有雪花
        return VQD_NORMAL;
    }

    const Mat imageDiffSquare = imageDiff.mul(imageDiff);                 // 平方差分图像
    const Scalar MSE = mean(imageDiffSquare);                             // 计算MSE
    const double PSNR = 10 * log(this->EPS + 255 * 255 / (MSE.val[0] + this->EPS));   // 计算PSNR
    
    const float psnrThreshold = 80.0F;                                    // PSNR阈值

    return (PSNR < psnrThreshold) ? VQD_SNOW : VQD_NORMAL;
}
/**********************************************************/
/*                     偏色异常检测                        */
/**********************************************************/
// 《面向视频监控的视频质量检测系统的设计与开发》 P33
// OPENCV: L∈[0, 255]   a∈[0, 255],     b∈[0, 255]
// CIELab: L∈[0, 100]   a∈[-128, 127]   b∈[-128, 127]
int ColourCast::detecteSigleImage(const Mat& imageColor) {
    Mat imageLab;
    cvtColor(imageColor, imageLab, COLOR_BGR2Lab);           // BRG转CIELab

    const Scalar mean, stddev;
    meanStdDev(imageLab, mean, stddev);                 // 计算各通道均值、方差
    
    //const double meanL = mean.val[0] * 100 / 255;
    const double meanA = mean.val[1] - 128;
    const double meanB = mean.val[2] - 128;

    //const double std2L = stddev.val[0] * 100 / 255;
    const double std2A = stddev.val[1];
    const double std2B = stddev.val[2];

    const double D = meanA * meanA + meanB * meanB;
    const double M = std2A * std2A + std2B * std2B + this->EPS;
    const double K = sqrt(D / M);
    
    const double colourCastThreshold = 1.0;

    //if (K > colourCastThreshold) {
    //    if (meanA >= abs(meanB)) {
    //        cout << "偏红！" << endl;
    //    } else if (meanB >= abs(meanA)) {
    //        cout << "偏黄！" << endl;
    //    } else if (-meanA >= abs(meanB)) {
    //        cout << "偏绿！" << endl;
    //    } else if (-meanB >= abs(meanA)) {
    //        cout << "偏蓝！" << endl;
    //    }
    //}

    return (K > colourCastThreshold) ? VQD_COLOURCAST : VQD_NORMAL;
}

/**********************************************************/
/*                     亮度异常检测                        */
/**********************************************************/
/*
// 方法一：《亮度异常检测方法》  基于直方图的方法可能会出现由偏色引起的误判
int Luminance::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray;
    cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);

    const int channels = 0;                             // 灰度图像
    const int histSize = 3;                             // 三个部分
    const float hranges[4] = { 0, 51, 203, 255 };             // 0-51, 52-203, 204-255
    const float* ranges[1] = {hranges};
    Mat hist;                                           // 直方统计结果

    calcHist(&imageColor, 1, &channels, Mat(), hist, 1, &histSize, ranges, false);

    const float sumFront = hist.at<float>(0);                 // 靠前区域(0-51)
    const float sumMiddle = hist.at<float>(1);                // 中间区域(52-203)
    const float sumRear = hist.at<float>(2);                  // 靠后区域(204-255)
    const float sum = sumFront + sumRear + sumMiddle + this->EPS;

    const double alpha = (0.2 * (sumFront + sumRear) + 0.6 * sumMiddle) / sum;

    const double luminanceThreshold = 0.44;
    //if (alpha < luminanceThreshold) {
    //    if (sumFront > sumRear) {
    //        cout << "过暗！" << endl;
    //    } else {
    //        cout << "过亮！" << endl;
    //    }
    //}

    return (alpha < luminanceThreshold) ? VQD_LUMINANCE : VQD_NORMAL;
}
*/
// 方法二：《基于多路实时流的视频图像质量检测的研究与实现》 P18
int Luminance::detecteSigleImage(const Mat& image) {
    Mat imageHsv;
    cvtColor(image, imageHsv, COLOR_BGR2HSV);           // BGR转HSV

    const Scalar mean, stddev;
    meanStdDev(imageHsv, mean, stddev);                 // 计算各通道均值、方差

    const double meanLuminance = mean.val[2];           // 亮度V通道均值

    const double bottomThreshold = 20;
    const double highThreshold = 230;

    //bool flag = true;
    //if (meanLuminance < bottomThreshold) {              // 小于底阈值--->过暗
    //    cout << "过暗！" << endl;
    //} else if (meanLuminance > highThreshold) {         // 大于高阈值--->过亮
    //    cout << "过亮！" << endl;
    //} else {
    //    flag = false;
    //}

    return ((meanLuminance < bottomThreshold) || (meanLuminance > highThreshold)) ? VQD_LUMINANCE : VQD_NORMAL;
}

int MasaicBlock::detecteSigleImage(const Mat& imageColor) {
    Mat imageGray, imageCanny;
    cvtColor(imageColor, imageGray, COLOR_BGR2GRAY);

    const double lowThresh = 40.0;
    const double highThresh = 200.0;
    Canny(imageGray, imageCanny, lowThresh, highThresh, 3);

    vector<vector<Point> > contours;
    findContours(imageCanny, contours, RETR_LIST, CHAIN_APPROX_NONE);
    
    const double lowSideRatio = 0.5;                        // 侧边比值（低阈值）
    const double highSideRatio = 1.5;                       // 侧边比值（高阈值）

    const double lowArea = 40;                              // 面积（低阈值）
    const double highArea = 500;                            // 面积（高阈值）

    const double lowAreaRatio = 0.5;                        // 面积比值（低阈值）
    const double highAreaRatio = 1.5;                       // 面积比值（高阈值）

    const double angleThresh = 1.0;                         // 角度阈值

    int masaicCount = 0;                                    // 马赛克个数

    for (size_t i = 0; i < contours.size(); i++) {
        Rect rect = boundingRect(contours[i]);              // 正矩形，无角偏
        RotatedRect rrect = minAreaRect(contours[i]);       // 最小外接矩形，有角偏

        double heightRatio = rect.height / (rrect.size.height + this->EPS);
        double widthRatio = rect.width / (rrect.size.width + this->EPS);

        double areaRect = rect.width * rect.height;
        double areaRRect = rrect.size.width * rrect.size.height;

        double areaRatio = areaRect / (areaRRect + this->EPS);

        if (abs(rrect.angle) < angleThresh) {
            if ((heightRatio > lowSideRatio && heightRatio < highSideRatio)         \
                && (widthRatio > lowSideRatio && widthRatio < highSideRatio)        \
                && (areaRect > lowArea && areaRect < highArea)                      \
                && (areaRatio > lowAreaRatio && areaRatio < highAreaRatio)
                ) {
                masaicCount++;
                //drawContours(imageColor, contours, i, Scalar(255, 0, 0), 1);
            }
        }
    }
    //imshow("canny", imageCanny);
    //imshow("masaic", imageColor);
    //waitKey();

    const int masaicThresh = 20;                // 马赛克个数阈值

    return (masaicCount > masaicThresh) ? VQD_BLUR : VQD_NORMAL;
}