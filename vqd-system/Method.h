﻿#ifndef _METHOD_H
#define _METHOD_H

#include <iostream>
#include <opencv2/core/core.hpp>

namespace vqd {
    /**********************************************************/
    enum VQD {
        VQD_NORMAL = 0,     // 正常情况
        VQD_BLACK,          // 黑屏
        VQD_FREEZE,         // 静帧(画面冻结、丢失)
        VQD_BLUR,           // 模糊
        VQD_COLOURCAST,     // 偏色
        VQD_SNOW,           // 雪花噪声
        VQD_LUMINANCE       // 亮度异常
    };
    // base class
    class Method {
    public:
        static const float EPS;

        virtual ~Method() {};
        // overload
        int detecte(const cv::Mat& imageColor) {
            return detecteSigleImage(imageColor);
        }
        int detecte(const cv::Mat& imgColorPrior, const cv::Mat& imageColorNext) {
            return detecteMultiImage(imgColorPrior, imageColorNext);
        }
    private:
        virtual int detecteSigleImage(const cv::Mat&) { return VQD_NORMAL; }
        virtual int detecteMultiImage(const cv::Mat&, const cv::Mat&) { return VQD_NORMAL; }
    };

    // black screen detection
    class BlackScreen : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };

    // frame freeze detection
    class FrameFreeze : public Method {
    private:
        virtual int detecteMultiImage(const cv::Mat&, const cv::Mat&) override;
    };

    // blur detection
    class FocusBlur : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };

    // snow noise detection
    class SnowNoise : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };

    // colour cast detection
    class ColourCast : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };

    // luminance anomaly detection
    class Luminance : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };

    // masaic block detection
    class MasaicBlock : public Method {
    private:
        virtual int detecteSigleImage(const cv::Mat&) override;
    };
}

#endif // _METHOD_H