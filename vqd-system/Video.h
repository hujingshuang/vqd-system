#ifndef _VIDEO_H
#define _VIDEO_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>

class Video {
private:
    cv::VideoCapture capture;
public:
    static Video& getVideo(const std::string& pathname) {
        static Video video(pathname);
        return video;
    }
    
    Video(const std::string& pathname) {
        capture.open(pathname);
    }
    
    void getFrame(std::vector<cv::Mat> sequence, const int* arrays) {
        
    }
};

#endif // _VIDEO_H