﻿#include <iostream>

#include "Method.h"
#include "Detector.h"
#include "tags.h"
#include <opencv2/highgui/highgui.hpp>

#define USE_ZH

using namespace cv;
using namespace std;
using namespace vqd;

static int frameCount = 0;                 // 帧计数器

void on_trackbar(int alpha_slider, void*) {
    frameCount = alpha_slider;
}

int main() {
    const int imageNums = 5;                                        // 队列池大小
    Detector& detector = Detector::getDetector(imageNums);          // 检测器
    

    SnowNoise* snownoise = new SnowNoise();                         // 雪花检测
    FocusBlur* focusblur = new FocusBlur();                         // 模糊检测
    MasaicBlock* masaic = new MasaicBlock();                        // 马赛克检测
    Luminance* luminance = new Luminance();                         // 亮度异常
    ColourCast* colourcast = new ColourCast();                      // 偏色异常
    BlackScreen* blackscreen = new BlackScreen();                   // 黑屏检测
    FrameFreeze* framefreeze = new FrameFreeze();                   // 静帧

    // 添加
    detector.attach(focusblur);
    detector.attach(masaic);
    detector.attach(snownoise);
    detector.attach(colourcast);
    detector.attach(luminance);
    detector.attach(blackscreen);
    detector.attach(framefreeze);       // 放在最后
    
    VideoCapture capture("../dataset/TestVideo.avi");
    Mat firstframe;
    capture >> firstframe;
    detector.setImage(firstframe);

    const double frameTotal = capture.get(CV_CAP_PROP_FRAME_COUNT);
    const int interval = 2;             // 帧间隔

#ifdef USE_ZH
    Tags tags;
#endif

    const string windowName = "vqd-system";
    const string trackbarName = "Track";
    namedWindow(windowName, 1);

    int alpha_slider = 0;
    const int alpha_slider_max = (int)frameTotal;
    createTrackbar(trackbarName, windowName, &alpha_slider, alpha_slider_max, on_trackbar);

    while (1) {
        Mat frame;
        if (frameCount > frameTotal) {
            break;
        }
        setTrackbarPos(trackbarName, windowName, frameCount);
        frameCount += interval;
        capture.set(CV_CAP_PROP_POS_FRAMES, frameCount);
        capture >> frame;
        if (frame.empty()) {
            break;
        }
        detector.setImage(frame);
        detector.detecte();

#ifdef USE_ZH
        tags.addTags(frame, detector.getResult());
#endif

        imshow("vqd-system", frame);
        waitKey(1);
    }

    destroyWindow("vqd-system");
    capture.release();
    
    delete snownoise;
    delete luminance;
    delete colourcast;
    delete blackscreen;
    delete focusblur;

    return 0;
}