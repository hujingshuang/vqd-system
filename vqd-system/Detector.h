﻿#ifndef _DETECTOR_H
#define _DETECTOR_H

#include <iostream>
#include <deque>
#include <list>
#include <vector>

#include <opencv2/core/core.hpp>
#include "Method.h"

namespace vqd {
    // 检测器
    class Detector {

    private:
        std::deque<cv::Mat> imageSquence;
        std::deque<std::vector<int> > vqdResult;
        std::list<Method*> lst;         // 检测方法列表

        Detector(const int nums) {
            imageSquence.resize(nums);
            vqdResult.resize(nums);
        }
        ~Detector() {}
        Detector(const Detector&);
        Detector& operator=(const Detector);

    public:
        static Detector& getDetector(const int nums) {
            static Detector detector(nums);
            return detector;
        }

        // 添加检测方法
        void attach(Method* method) {
            lst.push_back(method);
        }

        // 移除检测方法
        void detach(Method* method) {
            lst.remove(method);
        }

        // 设置更新图像
        void setImage(const cv::Mat& image) {
            imageSquence.pop_back();
            imageSquence.push_front(image);
        }

        // 检测方法
        void detecte() {
            std::vector<int> tmpResult(8, 0);
            size_t size = lst.size();
            if (size != 0) {
                std::list<Method*>::iterator it = lst.begin();
                for (int i = 0; i < size - 1; i++) {
                    tmpResult.at( (*it++)->detecte( *(imageSquence.begin()) ) ) = 1;
                }
                tmpResult.at((*it)->detecte(imageSquence.at(0), imageSquence.at(1))) = 1;
            }
            vqdResult.pop_back();
            vqdResult.push_front(tmpResult);
        }

        std::vector<int> getResult() {
            return *(vqdResult.begin());
        }
    };
}

#endif // _DETECTOR_H